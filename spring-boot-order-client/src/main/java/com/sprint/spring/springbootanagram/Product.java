package com.sprint.spring.springbootanagram;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Product extends AbstractDocument {

    private String name, description;
    private BigDecimal price;
    private Map<String, String> attributes = new HashMap<>();

    public Product(String name, BigDecimal price) {
        this(name, price, null);
    }

    public Product() {
    }

    
    //@PersistenceConstructor
    public Product(String name, BigDecimal price, String description) {

        this.name = name;
        this.price = price;
        this.description = description;
    }

    public void setAttribute(String name, String value) {

        if (value == null) {
            this.attributes.remove(value);
        } else {
            this.attributes.put(name, value);
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Map<String, String> getAttributes() {
        return Collections.unmodifiableMap(attributes);
    }

    public BigDecimal getPrice() {
        return price;
    }
}
