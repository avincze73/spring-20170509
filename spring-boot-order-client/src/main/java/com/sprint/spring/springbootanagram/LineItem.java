package com.sprint.spring.springbootanagram;

import java.math.BigDecimal;

import org.springframework.data.mongodb.core.mapping.DBRef;

public class LineItem extends AbstractDocument {

    @DBRef
    private Product product;
    private BigDecimal price;
    private int amount;

    public LineItem(Product product) {
        this(product, 1);
    }

    public LineItem(Product product, int amount) {

        this.product = product;
        this.amount = amount;
        this.price = product.getPrice();
    }

    public LineItem() {

    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public int getAmount() {
        return amount;
    }

    public BigDecimal getUnitPrice() {
        return price;
    }

    public BigDecimal getTotal() {
        return price.multiply(BigDecimal.valueOf(amount));
    }
}
