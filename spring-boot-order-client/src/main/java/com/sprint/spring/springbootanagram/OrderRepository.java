package com.sprint.spring.springbootanagram;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface OrderRepository extends MongoRepository<Order, Long> {

	
	List<Order> findByCustomer(Customer customer);
}
