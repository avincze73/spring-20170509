package com.sprint.spring.springbootanagram;

import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
//Spring Boot will handle those repositories automatically as long as they are included in the same package 
//(or a sub-package) of your @SpringBootApplication class. 
//For more control over the registration process, you can use the @EnableMongoRepositories annotation.
public class Application implements CommandLineRunner {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ProductRepository productRepository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        customerRepository.deleteAll();
        Customer customer = new Customer("a", "b", new EmailAddress("a@gmail.com"));
        customer.getAddresses().add(new Address("s1", "c1", "c1"));
        customerRepository.save(customer);

        productRepository.deleteAll();
        Product product = new Product("Camera bag", new BigDecimal(49.99));
        product.setAttribute("attr1", "attr1-value");
        productRepository.save(product);

    }

}
