package com.sprint.spring.springbootanagram;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository extends MongoRepository<Customer, Long> {

    Customer findByEmailAddress(EmailAddress emailAddress);
}
