/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.springbootanagram;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author avincze
 */
@RestController
public class OrderController {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderRepository orderRepository;

    @GetMapping(path = "/customer")
    public ResponseEntity<?> getCustomer() {
        return new ResponseEntity<>(customerRepository.findAll().get(0), HttpStatus.OK);
    }

    @RequestMapping(value = "/")
    public String home() {
        //log.info("Access /");
        return "Hi!";
    }

    @GetMapping(path = "/product")
    public ResponseEntity<?> getProduct() {
        return new ResponseEntity<>(productRepository.findAll().get(0), HttpStatus.OK);
    }

    @PostMapping(path = "/order/{customerId}/{productId}/{amount}")
    @ResponseStatus(HttpStatus.OK)
    public void postOrder(@PathVariable("customerId") Long customerId,
            @PathVariable("productId") Long productId, @PathVariable("amount") int amount) {
        Customer customer = customerRepository.findOne(customerId);
        System.out.println(customer.getId().longValue());
        Product product = productRepository.findOne(productId);
        System.out.println(product.getId().longValue());

        Order order = new Order(customer, null);
        order.add(new LineItem(product, amount));
        orderRepository.save(order);
    }
}
