package com.sprint.spring.emailservice;

import com.sprint.spring.spring.anagram.common.AnagramResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class AnagramMessageListener {

    private static final Logger log = LoggerFactory.getLogger(AnagramMessageListener.class);

    @RabbitListener(queues = EmailServiceApplication.QUEUE_NAME)
    public void receiveMessage(final AnagramResponse message) {
        log.info("Received message: {}", message.toString());
        log.info(message.getOriginal() + ": " + message.getAnagramList());
    }

}
