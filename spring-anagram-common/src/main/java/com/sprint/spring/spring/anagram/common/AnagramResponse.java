/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.spring.anagram.common;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author avincze
 */
public class AnagramResponse {

    private String original;
    private List<String> anagramList;

    public AnagramResponse(String original, List<String> anagramList) {
        this.original = original;
        this.anagramList = anagramList;
    }

    public AnagramResponse(String original) {
        this.original = original;
        this.anagramList = new ArrayList<>();
    }

    public AnagramResponse() {
        this.anagramList = new ArrayList<>();
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public List<String> getAnagramList() {
        return anagramList;
    }

    public void setAnagramList(List<String> anagramList) {
        this.anagramList = anagramList;
    }

}
