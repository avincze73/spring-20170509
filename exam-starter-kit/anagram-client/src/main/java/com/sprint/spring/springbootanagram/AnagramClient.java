/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.springbootanagram;

import org.springframework.cloud.netflix.feign.FeignClient;

/**
 *
 * @author avincze
 */
@FeignClient(name = "anagram-service", 
        configuration = FeignClientConfiguration.class)
public interface AnagramClient extends RemoteAnagramController {
}
