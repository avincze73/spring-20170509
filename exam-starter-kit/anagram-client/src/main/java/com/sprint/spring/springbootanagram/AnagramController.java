/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.springbootanagram;

import com.sprint.spring.spring.anagram.common.AnagramResponse;
import java.util.Arrays;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author avincze
 */
@RestController
public class AnagramController {

    @Autowired
    private AnagramClient anagramClient;

  
    public AnagramController(AnagramClient anagramClient) {
        this.anagramClient = anagramClient;
    }

    @GetMapping(value = "/{word}")
    public ResponseEntity<AnagramResponse> findFeign(@PathVariable("word") String word) {
        return anagramClient.find(word);
    }

  
}
