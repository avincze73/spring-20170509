/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author avincze
 */
@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    
    
    @RequestMapping(value = "/account/{username}", method = RequestMethod.GET)
    public ResponseEntity<Account> findAccount(@PathVariable("username") String username) {
        try {
            Account account = accountService.findByAccountByUserName(username);
            return new ResponseEntity<>(account, HttpStatus.OK);
        } catch (AccountNotFoundException ex) {
            Logger.getLogger(AccountController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity(new CustomErrorType("Account with id " + username
                    + " not found"), HttpStatus.NOT_FOUND);
        }
    }

}
