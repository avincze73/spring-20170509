/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author avincze
 */
@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    public Account findByAccountByUserName(String userName) throws AccountNotFoundException {
        Account account = accountRepository.findOne(userName);
        if (account == null) {
            throw new AccountNotFoundException();
        }
        return account;
    }

}
