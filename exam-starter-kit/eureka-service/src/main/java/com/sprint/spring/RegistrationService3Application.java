package com.sprint.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class RegistrationService3Application {

	public static void main(String[] args) {
		SpringApplication.run(RegistrationService3Application.class, args);
	}
}
