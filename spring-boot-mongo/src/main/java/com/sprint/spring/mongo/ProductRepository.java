package com.sprint.spring.mongo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<Product, Long> {

	List<Product> findByDescriptionContaining(String description);

	List<Product> findByAttributes(String key, String value);
}
