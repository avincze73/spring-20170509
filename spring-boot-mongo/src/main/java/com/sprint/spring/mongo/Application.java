package com.sprint.spring.mongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
//Spring Boot will handle those repositories automatically as long as they are included in the same package 
//(or a sub-package) of your @SpringBootApplication class. 
//For more control over the registration process, you can use the @EnableMongoRepositories annotation.
public class Application implements CommandLineRunner {

	@Autowired
	private CustomerRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		repository.deleteAll();

		// save a couple of customers
                Customer customer1 = new Customer("Alice", "Smith", new EmailAddress("a@gmail.com"));
                customer1.getAddresses().add(new Address("st1", "city1", "country1"));
		//repository.save(customer1);
		//repository.save(new Customer("Bob", "Smith", new EmailAddress("b@gmail.com")));

		// fetch all customers
		System.out.println("Customers found with findAll():");
		System.out.println("-------------------------------");
		for (Customer customer : repository.findAll()) {
			System.out.println(customer.getId());
		}
		System.out.println();

		// fetch an individual customer
		System.out.println("Customer found with findByFirstName('Alice'):");
		System.out.println("--------------------------------");
		//System.out.println(repository.findByFirstName("Alice"));

		System.out.println("Customers found with findByLastName('Smith'):");
		System.out.println("--------------------------------");
		//for (Customer customer : repository.findByLastName("Smith")) {
		//	System.out.println(customer);
		//}

	}

}
