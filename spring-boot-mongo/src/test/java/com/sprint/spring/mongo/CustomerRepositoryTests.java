package com.sprint.spring.mongo;

import com.sprint.spring.mongo.EmailAddress;
import com.sprint.spring.mongo.Customer;
import com.sprint.spring.mongo.CustomerRepository;
import org.springframework.dao.DuplicateKeyException;
import static org.assertj.core.api.Assertions.*;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerRepositoryTests {

    @Autowired
    CustomerRepository repository;

    @Before
    public void setUp() {
        repository.deleteAll();
    }

    @Test
    public void savesCustomer() {
        Customer customer = new Customer("a", "b", new EmailAddress("a@gmail.com"));
        repository.save(customer);
        assertThat(customer.getId()).isNotNull();
    }

    @Test
    public void findCustomer() {
        Customer customer = new Customer("a", "b", new EmailAddress("a@gmail.com"));
        repository.save(customer);
        Customer saved = repository.findByEmailAddress(new EmailAddress("a@gmail.com"));
        assertThat(saved.getEmailAddress().toString()).isEqualTo("a@gmail.com");
    }

    @Test(expected = DuplicateKeyException.class)
    public void preventsDuplicateEmail() {

        Customer customer = new Customer("a", "b", new EmailAddress("a@gmail.com"));
        repository.save(customer);
        Customer other = new Customer("c", "d", new EmailAddress("a@gmail.com"));
        repository.save(other);
    }

  
}
