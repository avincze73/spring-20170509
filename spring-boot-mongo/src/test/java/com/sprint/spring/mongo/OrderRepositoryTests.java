package com.sprint.spring.mongo;

import com.sprint.spring.mongo.EmailAddress;
import com.sprint.spring.mongo.Customer;
import com.sprint.spring.mongo.CustomerRepository;
import java.math.BigDecimal;
import org.springframework.dao.DuplicateKeyException;
import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderRepositoryTests {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ProductRepository productRepository;

    @Before
    public void setUp() {
        orderRepository.deleteAll();
        customerRepository.deleteAll();
        productRepository.deleteAll();
    }

    @Test
    public void savesOrder() {
        Product product = new Product("Camera bag", new BigDecimal(49.99));
        
        product.setAttribute("attr1", "attr1-value");
        product = productRepository.save(product);

        Customer customer = new Customer("a", "b", new EmailAddress("a@gmail.com"));
        customer.getAddresses().add(new Address("street1", "city1", "country1"));
        customerRepository.save(customer);

        Order order = new Order(customer, customer.getAddresses().iterator().next());
        order.add(new LineItem(product));

        order = orderRepository.save(order);
        assertThat(order.getId()).isNotNull();

    }

}
