package com.sprint.spring.mongo;

import java.math.BigDecimal;
import java.util.List;
import org.springframework.dao.DuplicateKeyException;
import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductRepositoryTests {

    @Autowired
    ProductRepository repository;

    @Before
    public void setUp() {
        repository.deleteAll();
    }

    @Test
    public void createProduct() {
        Product product = new Product("Camera bag", new BigDecimal(49.99));
        product.setAttribute("attr1", "attr1-value");
        product = repository.save(product);
        assertThat(product.getId()).isNotNull();
    }

    @Test
    public void findProductsByDescription() {

        Product product = new Product("Camera bag", new BigDecimal(49.99), "desc1");
        product.setAttribute("attr1", "attr1-value");
        product = repository.save(product);
        assertThat(product.getId()).isNotNull();

        List<Product> other = repository.findByDescriptionContaining("des");
        assertThat(other.size()).isEqualTo(1);
    }

    @Ignore
    @Test
    public void findsProductsByAttributes() {
        Product product = new Product("Camera bag", new BigDecimal(49.99), "desc1");
        product.setAttribute("attr1", "attr1-value");
        product = repository.save(product);
        List<Product> products = repository.findByAttributes("attr1", "attr1-value");
        assertThat(products.size()).isEqualTo(1);
    }

}
