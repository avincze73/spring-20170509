package com.sprint.spring.redis.repo;

import com.sprint.spring.redis.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Map;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

    private static final String KEY = "a:Employee";

    private RedisTemplate<String, Object> redisTemplate;
    private HashOperations hashOperations;

    @Autowired
    public EmployeeRepositoryImpl(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @PostConstruct
    private void init() {
        hashOperations = redisTemplate.opsForHash();
    }

    public void save(final Employee student) {
        hashOperations.put(KEY, student.getId(), student);
    }

    public void update(final Employee student) {
        hashOperations.put(KEY, student.getId(), student);
    }

    public Employee find(final String id) {
        return (Employee) hashOperations.get(KEY, id);
    }

    public Map<Object, Object> findAll() {
        return hashOperations.entries(KEY);
    }

    public void delete(final String id) {
        hashOperations.delete(KEY, id);
    }
}
