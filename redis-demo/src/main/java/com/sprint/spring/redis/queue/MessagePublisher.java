package com.sprint.spring.redis.queue;

public interface MessagePublisher {

    void publish(final String message);
}
