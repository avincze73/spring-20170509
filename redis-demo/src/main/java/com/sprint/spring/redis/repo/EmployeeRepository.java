package com.sprint.spring.redis.repo;


import com.sprint.spring.redis.model.Employee;
import java.util.Map;

public interface EmployeeRepository {

    void save(Employee person);

    void update(Employee student);

    Employee find(String id);

    Map<Object, Object> findAll();

    void delete(String id);
}
