package com.sprint.spring.redis.repo;

import com.sprint.spring.redis.config.RedisConfig;
import com.sprint.spring.redis.model.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RedisConfig.class)
public class EmployeeRepositoryIntegrationTest {

    @Autowired
    private EmployeeRepository repository;

    @Test
    public void whenSavingEmployee_thenAvailableOnRetrieval() throws Exception {
        final Employee student = 
                new Employee("Eng2015001", "John Doe", Employee.Gender.MALE, 1);
        repository.save(student);
        final Employee retrievedEmployee = 
                repository.find(student.getId());
        assertEquals(student.getId(), retrievedEmployee.getId());
    }

    @Test
    public void whenUpdatingEmployee_thenAvailableOnRetrieval() throws Exception {
        final Employee student = new Employee("Eng2015001", "John Doe", Employee.Gender.MALE, 1);
        repository.save(student);
        student.setName("Richard Watson");
        repository.save(student);
        final Employee retrievedEmployee = repository.find(student.getId());
        assertEquals(student.getName(), retrievedEmployee.getName());
    }

    @Test
    public void whenSavingEmployees_thenAllShouldAvailableOnRetrieval() throws Exception {
        final Employee engEmployee = new Employee("Eng2015001", "John Doe", Employee.Gender.MALE, 1);
        final Employee medEmployee = new Employee("Med2015001", "Gareth Houston", Employee.Gender.MALE, 2);
        repository.save(engEmployee);
        repository.save(medEmployee);
        final Map<Object, Object> retrievedEmployee = repository.findAll();
        assertEquals(retrievedEmployee.size(), 2);
    }

    @Test
    public void whenDeletingEmployee_thenNotAvailableOnRetrieval() throws Exception {
        final Employee student = new Employee("Eng2015001", "John Doe", Employee.Gender.MALE, 1);
        repository.save(student);
        repository.delete(student.getId());
        final Employee retrievedEmployee = repository.find(student.getId());
        assertNull(retrievedEmployee);
    }
}