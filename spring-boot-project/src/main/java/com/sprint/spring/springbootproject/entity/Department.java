package com.sprint.spring.springbootproject.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;

@Entity
public class Department extends EvoEntity {

	@Basic(optional = false)
	@Column(name = "name")
	private String name;
	@Embedded
	private Address address;

	public Department() {
		// TODO Auto-generated constructor stub
		this(null, null);
	}

	public Department(String name, Address address) {
		super();
		this.name = name;
		this.address = address;
	}

	public Department(String name) {
		// TODO Auto-generated constructor stub
		this(name, null);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
