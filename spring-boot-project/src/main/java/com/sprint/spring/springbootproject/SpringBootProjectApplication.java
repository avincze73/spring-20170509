package com.sprint.spring.springbootproject;

import com.sprint.spring.springbootproject.entity.Role;
import com.sprint.spring.springbootproject.repository.RoleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringBootProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootProjectApplication.class, args);
	}
        
        @Bean
        CommandLineRunner init(RoleRepository repository){
            return (args)->{
                Role role = new Role("admin");
                Role role2 = new Role("user");
                repository.save(role);
                repository.save(role2);
            };
        }
}
