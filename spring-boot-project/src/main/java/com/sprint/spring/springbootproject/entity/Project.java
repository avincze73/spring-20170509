package com.sprint.spring.springbootproject.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Project extends EvoEntity {

	@Basic(optional = false)
	@Column(name = "name")
	private String name;
	@Temporal(TemporalType.DATE)
	@Column(name = "startDate")
	private Date startDate;
	@Temporal(TemporalType.DATE)
	@Column(name = "endDate")
	private Date endDate;

	@ManyToOne(optional = false)
	@JoinColumn(name = "ownerId", referencedColumnName = "id")
	private Employee owner;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "projectId", referencedColumnName = "id")
	private List<Task> taskList;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Employee getOwner() {
		return owner;
	}

	public void setOwner(Employee owner) {
		this.owner = owner;
	}

	public List<Task> getTaskList() {
		return taskList;
	}

	public void setTaskList(List<Task> taskList) {
		this.taskList = taskList;
	}

}
