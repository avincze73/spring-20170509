/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.springbootproject.controller;

import com.sprint.spring.springbootproject.entity.Role;
import com.sprint.spring.springbootproject.repository.RoleRepository;
import com.sprint.spring.springbootproject.service.RoleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author avincze
 */
@RestController
@RequestMapping(path = "/roles", method = RequestMethod.GET)
public class RoleController {

    @Autowired
    private RoleService roleService;

    @RequestMapping(value="/a", method = RequestMethod.GET)
    public ResponseEntity<?> findAll() {
        List<Role> roleList = roleService.findAll();
        if (!roleList.isEmpty()) {
            //return new ResponseEntity<>(new CustomException("No roles found."), HttpStatus.NOT_FOUND);
            throw new RestException(0, "Not found", "Role not found");
        }
        return new ResponseEntity<>(roleList, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> find(@PathVariable("id") Long id) {
        Role role = roleService.find(id);
        if (role == null) {
            throw new RestException(0, "Not found", "Role not found");
        }
        return new ResponseEntity<>(role, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        roleService.delete(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> add(@RequestBody Role role, UriComponentsBuilder ucBuilder) {
        roleService.save(role);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/roles/{id}").buildAndExpand(role.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
    
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> update(@RequestBody Role role) {
        roleService.save(role);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
