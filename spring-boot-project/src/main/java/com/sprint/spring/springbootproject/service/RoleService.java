/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.springbootproject.service;

import com.sprint.spring.springbootproject.entity.Role;
import com.sprint.spring.springbootproject.repository.RoleRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author avincze
 */
@Service
@Transactional
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;
    
    public void save(Role role){
        roleRepository.save(role);
    }
    
    public void delete(Long id){
        roleRepository.delete(id);
    }
    
    public List<Role> findAll(){
        return roleRepository.findAll();
    }
   
    public Role find(Long id){
        return roleRepository.findOne(id);
    }
}
