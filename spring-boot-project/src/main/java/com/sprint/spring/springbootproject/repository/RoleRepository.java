package com.sprint.spring.springbootproject.repository;

import com.sprint.spring.springbootproject.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RoleRepository extends JpaRepository<Role, Long> {

}
