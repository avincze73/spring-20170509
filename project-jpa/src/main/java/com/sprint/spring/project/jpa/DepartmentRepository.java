/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.project.jpa;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author avincze
 */
public interface DepartmentRepository
        extends CrudRepository<Department, Long> {

    List<Department> findDepartmentByName(String name);
}
