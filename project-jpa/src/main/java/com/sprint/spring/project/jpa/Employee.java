package com.sprint.spring.project.jpa;

import com.sprint.spring.project.jpa.Address;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Employee extends EvoEntity {
	@Basic(optional = false)
	@Column(name = "firstName")
	private String firstName;
	@Basic(optional = false)
	@Column(name = "lastName")
	private String lastName;
	@Basic(optional = false)
	@Column(name = "userName")
	private String userName;
	@Basic(optional = false)
	@Column(name = "password")
	private String password;
	@Embedded
	private Address address;

	@ManyToOne(optional = true)
	@JoinColumn(name = "bossId", referencedColumnName = "id")
	private Employee boss;

	@ManyToOne(optional = true)
	@JoinColumn(name = "departmentId", referencedColumnName = "id")
	private Department department;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "employeeRoles", 
	joinColumns = @JoinColumn(name = "employeeId", referencedColumnName = "id"), 
	inverseJoinColumns = @JoinColumn(name = "roleId", referencedColumnName = "id"))
	private Set<Role> roleList;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Employee getBoss() {
		return boss;
	}

	public void setBoss(Employee boss) {
		this.boss = boss;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Set<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(Set<Role> roleList) {
		this.roleList = roleList;
	}

}
