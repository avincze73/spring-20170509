/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.project.jpa;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author avincze
 */
public class Main2 {
    public static void main(String[] args) {
        ApplicationContext context
                = new AnnotationConfigApplicationContext(JpaConfiguration.class);
        RoleService roleService = context.getBean(RoleService.class);
        roleService.add(new Role("user"));
        
        DepartmentService departmentService = context.getBean(DepartmentService.class);
        Department department = new Department("it");
        department.setAddress(new Address("a", "b", "c"));
        departmentService.add(department);
        
        department = new Department("hr");
        department.setAddress(new Address("e", "f", "g"));
        departmentService.add(department);
        
        departmentService.findByName("hr").forEach(System.out::println);
        
        
        
        
    }
    
}
