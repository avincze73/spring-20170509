package com.sprint.spring.project.jpa;

import org.springframework.data.repository.CrudRepository;


public interface RoleRepositorySpring extends CrudRepository<Role, Long> {

}
