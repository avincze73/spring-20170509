/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.project.jpa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author avincze
 */
@Service
@Transactional
public class DepartmentService {
    
    @Autowired
    private DepartmentRepository departmentRepository;
    
    public void add(Department department){
        departmentRepository.save(department);
        
    }
    
    public List<Department> findAll(){
        return new ArrayList<Department>((Collection<? extends Department>) departmentRepository.findAll());
    }
    
    public List<Department> findByName(String name){
        return departmentRepository.findDepartmentByName(name);
    }
}
