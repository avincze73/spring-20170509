/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.project.jpa;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author avincze
 */
@Configuration
@EnableJpaRepositories
@EnableTransactionManagement
@ComponentScan
public class JpaConfiguration {

    @Bean
    public DataSource datasource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/projectdb");
        dataSource.setUsername("spring");
        dataSource.setPassword("spring");
        return dataSource;
    }

    private Map<String, ?> jpaProperties() {
        Map<String, String> jpaMap = new HashMap<>();
        jpaMap.put("javax.persistence.schema-generation.database.action", "drop-and-create");
        jpaMap.put("hibernate.show_sql", "true");
        return jpaMap;
    }

    //This is for standalone application
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        factoryBean.setDataSource(datasource());
        factoryBean.setPackagesToScan("com.sprint.spring.project.jpa");
        factoryBean.setJpaPropertyMap(jpaProperties());
        return factoryBean;
    }

    @Bean
    //@Autowired
    public PlatformTransactionManager 
        transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = 
                new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

}
