/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.project.jpa;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

/**
 *
 * @author avincze
 */
public class Main {

    public static void main(String[] args) {
        ApplicationContext context
                = new AnnotationConfigApplicationContext(JpaConfiguration.class);

        EntityManager em = ((EntityManagerFactory) context.getBean("entityManagerFactory"))
                .createEntityManager();

        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        Role role1 = new Role("role1");
        Role role2 = new Role("role2");
        em.persist(role1);
        em.persist(role2);
        Department department1 = new Department("department1");
        department1.setAddress(new Address("zip1", "city1", "street1"));
        Department department2 = new Department("department2");
        department2.setAddress(new Address("zip2", "city2", "street2"));
        em.persist(department1);
        em.persist(department2);


        transaction.commit();
        
        
//        RoleService roleService = context.getBean(RoleService.class);
//        Role role = new Role("admin");
//        roleService.save(role);
    }

}
