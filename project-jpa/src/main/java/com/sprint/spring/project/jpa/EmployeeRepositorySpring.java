package com.sprint.spring.project.jpa;

import java.util.List;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.Repository;

public interface EmployeeRepositorySpring extends Repository<Employee, Long>{
	void save(Employee arg);
	List<Employee> findAll();
        
        List<Employee> findByFirstName(String firstName);
        
        @Query("select e from Employee e where e.firstName like 'fsn1'")
        List<Employee> findMyAlg();
        
	
}
