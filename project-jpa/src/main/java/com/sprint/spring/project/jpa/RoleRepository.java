/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.project.jpa;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author avincze
 */
@Repository
public class RoleRepository {

    @PersistenceContext
    private EntityManager em;

    public void add(Role role) {
        em.persist(role);
    }

    public List<Role> findAll() {
        return em.createQuery("select r from Role r", Role.class).getResultList();
    }
}
