/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.springbootanagram;

import com.sprint.spring.spring.anagram.common.AnagramResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;
/**
 *
 * @author avincze
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AnagramClientTest {
    
    @Autowired
    private TestRestTemplate testRestTemplate;
    
    @Test
    public void testGetAnagramOfAWord(){
        AnagramResponse response = testRestTemplate.getForObject("/secure", AnagramResponse.class);
        assertThat(response.getOriginal()).isEqualTo("secure");
        assertThat(response.getAnagramList().size()).isEqualTo(4);
        assertThat(response.getAnagramList()).contains("rescue");
    }
}
