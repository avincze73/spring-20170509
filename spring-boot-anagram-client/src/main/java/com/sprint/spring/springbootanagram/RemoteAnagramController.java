/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.springbootanagram;

import com.sprint.spring.spring.anagram.common.AnagramResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author avincze
 */
public interface RemoteAnagramController {

    @RequestMapping(value = "/anagram/{word}", method = RequestMethod.GET)
    public ResponseEntity<AnagramResponse> find(@PathVariable("word") String word);
}
