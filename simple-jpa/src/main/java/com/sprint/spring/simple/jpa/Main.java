/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.simple.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;

/**
 *
 * @author avincze
 */
public class Main {
    
    public static void main(String[] args) {
        
        ApplicationContext context = 
                new AnnotationConfigApplicationContext(JpaConfiguration.class);
        //EntityManagerFactory factory = 
        //        Persistence.createEntityManagerFactory("simple-jpa-pu");
        //EntityManager em = factory.createEntityManager();
        EntityManager em = 
                context.getBean(LocalContainerEntityManagerFactoryBean.class)
                .getObject().createEntityManager();
        em.getTransaction().begin();
        Employee employee = new Employee();
        employee.setId(1);
        employee.setFirstName("attila");
        em.persist(employee);
        em.getTransaction().commit();
        
        //factory.close();
    }
    
}
