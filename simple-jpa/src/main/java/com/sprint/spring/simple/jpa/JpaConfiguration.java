/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.simple.jpa;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;

/**
 *
 * @author avincze
 */
@Configuration
public class JpaConfiguration {

    @Bean
    public DataSource datasource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/employeedb");
        dataSource.setUsername("spring");
        dataSource.setPassword("spring");
        return dataSource;
    }

    private Map<String, ?> jpaProperties() {
        Map<String, String> jpaMap = new HashMap<>();
        jpaMap.put("javax.persistence.schema-generation.database.action", "drop-and-create");
        return jpaMap;
    }

    //@Bean
//    public LocalEntityManagerFactoryBean entityManagerFactory2() {
//        LocalEntityManagerFactoryBean factoryBean = new LocalEntityManagerFactoryBean();
//        factoryBean.setPersistenceUnitName("simple-jpa-pu");
//        return factoryBean;
//    }
    //This is for standalone application
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        factoryBean.setDataSource(datasource());
        factoryBean.setPackagesToScan("com.sprint.spring.simple.jpa");
        factoryBean.setJpaPropertyMap(jpaProperties());
        return factoryBean;
    }

}
