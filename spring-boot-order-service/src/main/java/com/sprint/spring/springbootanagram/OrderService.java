/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.springbootanagram;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author avincze
 */
@Service
public class OrderService {

    private final RestTemplate restTemplate;

    public OrderService(RestTemplate rest) {
        this.restTemplate = rest;
    }

    @HystrixCommand(fallbackMethod = "reliable")
    public void addOrder() {
        Customer customer = restTemplate
                .getForEntity("http://spring-boot-order-service/customer",
                        Customer.class).getBody();
        System.out.println(customer.getId().longValue());
        //Product product = restTemplate.getForEntity("http://localhost:1111/product",
        //        Product.class).getBody();
        //System.out.println(product.getId().longValue());
    }

    public String reliable() {
        return "Hystrix response";
    }
}
