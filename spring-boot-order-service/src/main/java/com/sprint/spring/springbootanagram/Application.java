package com.sprint.spring.springbootanagram;

import com.sprint.spring.spring.anagram.common.AnagramResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
@RibbonClient(name = "spring-boot-order-service", 
        configuration = OrderServiceConfiguration.class)
//Spring Boot will handle those repositories automatically as long as they are included in the same package 
//(or a sub-package) of your @SpringBootApplication class. 
//For more control over the registration process, you can use the @EnableMongoRepositories annotation.
public class Application {

    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Autowired
    private OrderService orderService;

    @PostMapping(path = "/order")
    @ResponseStatus(HttpStatus.OK)
    public void postOrder() {
        
//        restTemplate.postForLocation("http://localhost:1111/order/"
//                + customer.getId().longValue() + "/"
//                + product.getId().longValue() + "/12",
//                AnagramResponse.class);
        orderService.addOrder();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
