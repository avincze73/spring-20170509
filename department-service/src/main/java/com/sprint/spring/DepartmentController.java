/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author avincze
 */
@RestController
public class DepartmentController {

    @Autowired
    private IDepartmentService service;

    @RequestMapping(value = "/version", method = RequestMethod.GET)
    public String version() {
        return "1.2.3.4";
    }

    @GetMapping(value = "/departments/{id}")
    public ResponseEntity<Department> findDepartment(@PathVariable("id") Long id) {
        Department department = service.getById(id);
        if (department == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(department, HttpStatus.OK);
    }

    @RequestMapping(value = "/departments", method = RequestMethod.GET)
    public ResponseEntity<List<Department>> findDepartments() {
        List<Department> accounts = service.getAll();
        if (accounts.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }

    @RequestMapping(value = "/departments", method = RequestMethod.POST)
    public ResponseEntity<Void> createDepartment(@RequestBody Department department, UriComponentsBuilder ucBuilder) {
        service.save(new Department(department.getName(), department.getAddress()));
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/departments/{id}").buildAndExpand(department.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED); 
    }

    @RequestMapping(value = "/departments/{id}", method = RequestMethod.PUT)
    public void updateDeparment(@PathVariable("id") Long id, @RequestBody Department department) {
        Department original = service.getById(id);
        original.setAddress(department.getAddress());
        original.setName(department.getName());
        service.save(original);
    }

    @RequestMapping(value = "/departments/{id}", method = RequestMethod.DELETE)
    public void deleteDeparment(@PathVariable("id") Long id) {
        Department department = service.getById(id);
        service.delete(department);
    }
}
