package com.sprint.spring;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DepartmentsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DepartmentsApplication.class, args);
    }

    @Bean
    CommandLineRunner init(DepartmentRepository repository) {
        return (args) -> {
            Department d = new Department();
            d.setName("name1");
            d.setAddress(new Address("1111", "city 1", "street 1"));
            repository.save(d);

            d = new Department();
            d.setName("name2");
            d.setAddress(new Address("2222", "city 2", "street 2"));
            repository.save(d);
        };

    }
}
