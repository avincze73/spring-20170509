/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring;

import java.util.List;

/**
 *
 * @author avincze
 */
public interface IDepartmentService {

    Department getByName(String name);

    List<Department> getAll();

    void save(Department department);
    
    Department getById(Long id);
    
    void delete(Department department);
    
    boolean exists(Department department);

}
