package com.sprint.spring;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Address {

	@Basic(optional = false)
	@Column(name = "zip")
	private String zip;

	@Basic(optional = false)
	@Column(name = "city")
	private String city;

	@Basic(optional = false)
	@Column(name = "street")
	private String street;

	public Address() {
		// TODO Auto-generated constructor stub
		this(null, null, null);
	}

	public Address(String zip, String city, String street) {
		super();
		this.zip = zip;
		this.city = city;
		this.street = street;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

}
