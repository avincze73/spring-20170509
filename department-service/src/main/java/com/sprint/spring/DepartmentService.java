/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author avincze
 */
@Service
public class DepartmentService implements IDepartmentService {

    @Autowired
    private DepartmentRepository repository;

    @Override
    public Department getByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public List<Department> getAll() {
        return repository.findAll();
    }

    @Override
    public void save(Department department) {
        repository.save(department);
    }

    @Override
    public Department getById(Long id) {
        return repository.findOne(id);
    }

    @Override
    public void delete(Department department) {
        repository.delete(department);
    }

    @Override
    public boolean exists(Department department) {
        return repository.exists(department.getId());
    }

}
