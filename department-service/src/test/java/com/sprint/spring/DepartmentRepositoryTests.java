/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author avincze
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class DepartmentRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DepartmentRepository repository;

    @Test
    public void findByNameTest() {
        
        Department department = this.repository.findByName("name2");
        assertThat(department.getName()).isEqualTo("name2");
        assertThat(department.getAddress().getCity()).isEqualTo("city 2");
    }
    
    @Test
    public void persistTest() {
        
        Department a1 = new Department("name3", new Address("3333", "city 3", "street 3"));
        this.entityManager.persist(a1);
        Department department = this.repository.findByName("name3");
       
        assertThat(department.getName()).isEqualTo("name3");
        assertThat(department.getAddress().getCity()).isEqualTo("city 3");
    }
    
    @Test
    public void updateTest() {
        Department department = this.repository.findByName("name2");
        department.getAddress().setCity("city 22");
        department = this.repository.findByName("name2");
        assertThat(department.getName()).isEqualTo("name2");
        assertThat(department.getAddress().getCity()).isEqualTo("city 22");
    }
}
