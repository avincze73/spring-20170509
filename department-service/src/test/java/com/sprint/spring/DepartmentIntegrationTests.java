/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import org.springframework.restdocs.payload.JsonFieldType;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import org.springframework.restdocs.request.RequestDocumentation;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author avincze
 */
@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
//@DataJpaTest
//@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.AUTO_CONFIGURED)
//@SpringBootTest
//@ActiveProfiles( profiles={"local"})
//@SpringBootTest
@WebMvcTest(DepartmentController.class)
@AutoConfigureRestDocs(outputDir = "target/snippets")
public class DepartmentIntegrationTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IDepartmentService departmentService;

    @MockBean
    private DepartmentRepository departmentRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setup() {
        given(this.departmentService.getById(1L)).willReturn(new Department(1L, "name1", new Address("1111", "city 1", "street 1")));
    }

    @Test

    public void testGetDepartments() throws Exception {
        List<Department> departmentList = new ArrayList<>();
        departmentList.add(new Department(1L, "department 1", new Address("1111", "city 1", "street 1")));
        departmentList.add(new Department(2L, "department 2", new Address("2222", "city 2", "street 2")));

        when(departmentService.getAll()).thenReturn(departmentList);
        this.mvc.perform(get("/departments"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("department 1")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("department 2")))
                .andDo(
                        document("getdepartments", preprocessRequest(prettyPrint()),
                                preprocessResponse(prettyPrint()),
                                RequestDocumentation.requestParameters(),
                                responseFields(
                                        fieldWithPath("[].id").description("ID"),
                                        fieldWithPath("[].name").description("NAME"),
                                        fieldWithPath("[].address").description("ADDRESS")
                                                .type(Address.class).optional()
                                )));

        verify(departmentService, times(1)).getAll();
        verifyNoMoreInteractions(departmentService);
    }

    @Test
    public void testGetDepartment() throws Exception {
        Department department = new Department(2L, "department 2", new Address("1111", "city 1", "street 1"));

        when(departmentService.getById(2L)).thenReturn(department);

        this.mvc.perform(get("/departments/{id}", 2))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.name", is("department 2")))
                .andDo(
                        document("getdepartment",
                                preprocessRequest(prettyPrint()),
                                preprocessResponse(prettyPrint()),
                                RequestDocumentation.requestParameters(),
                                responseFields(
                                        fieldWithPath("id").description("ID"),
                                        fieldWithPath("name").description("NAME"),
                                        fieldWithPath("address").description("ADDRESS").type(Address.class)
                                )));

        verify(departmentService, times(1)).getById(2L);
        verifyNoMoreInteractions(departmentService);
    }

    @Test
    public void testGetDepartmentWithNotFound() throws Exception {
        when(departmentService.getById(1L)).thenReturn(null);
        this.mvc.perform(get("/departments/{id}", 1))
                .andExpect(status().isNotFound());
        verify(departmentService, times(1)).getById(1L);
        verifyNoMoreInteractions(departmentService);
    }

    @Ignore
    @Test
    public void testCreateDepartment() throws Exception {
        Department department = new Department(1L, "department 1", new Address("1111", "city 1", "street 1"));

        when(departmentService.exists(department)).thenReturn(false);
        doNothing().when(departmentService).save(department);

        this.mvc.perform(post("/departments").contentType(MediaType.APPLICATION_JSON).content(asJsonString(department)))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", containsString("/departments/")));

        //verify(departmentService, times(1)).exists(department);
        verify(departmentService, times(1)).save(department);
        verifyNoMoreInteractions(departmentService);
    }

    @Ignore
    @Test
    public void testUpdateDepartment() throws Exception {
        Department department = new Department(1L, "department 1", new Address("1111", "city 1", "street 1"));

        //when(departmentService.getById(1L)).thenReturn(department);
        //doNothing().when(departmentService).save(department);
        this.mvc.perform(
                put("/departments/{id}", department.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(department)))
                .andExpect(status().isOk());

        //verify(departmentService, times(1)).getById(department.getId());
        //verify(departmentService, times(1)).save(department);
        //verifyNoMoreInteractions(departmentService);
    }

    @Test
    public void testDeleteDepartment() throws Exception {
        Department department = new Department(1L, "department 1", new Address("1111", "city 1", "street 1"));

        when(departmentService.getById(1L)).thenReturn(department);
        doNothing().when(departmentService).delete(department);

        this.mvc.perform(
                delete("/departments/{id}", department.getId()))
                .andExpect(status().isOk());

        verify(departmentService, times(1)).getById(department.getId());
        verify(departmentService, times(1)).delete(department);
        verifyNoMoreInteractions(departmentService);
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Ignore
    @Test
    public void test22() throws Exception {

        this.mvc.perform(get("/departments/{id}", "1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name", is("name1")))
                .andDo(
                        document("finddepartment",
                                preprocessRequest(prettyPrint()),
                                preprocessResponse(prettyPrint()),
                                RequestDocumentation.requestParameters(),
                                responseFields(
                                        fieldWithPath("id").description("ID"),
                                        fieldWithPath("name").description("NAME"),
                                        fieldWithPath("description").description("DESCRIPTION").type(JsonFieldType.STRING).optional()
                                )));

    }
}
