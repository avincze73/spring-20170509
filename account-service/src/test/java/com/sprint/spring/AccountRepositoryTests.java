/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author avincze
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class AccountRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AccountRepository repository;

    @Test
    @Rollback(value = false)
    public void findByUsernameShouldReturnUser() {
        System.out.println(entityManager.
                getEntityManager()
                .createQuery("select e from Account e", Account.class)
                .getResultList().size());
        Account a1 = new Account();
        a1.setUsername("user3");
        a1.setPassword("user3");
        a1.setFullname("user 3");
        this.entityManager.persist(a1);
        Account user = this.repository.findByUsername("user2");
        entityManager.flush();

        
        assertThat(user.getUsername()).isEqualTo("user2");
        assertThat(user.getFullname()).isEqualTo("user 2");
    }
}
