/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author avincze
 */
@Service
public class AccountService implements IAccountService{

    @Autowired
    private AccountRepository repository;
    
    @Override
    public Account getByUserName(String userName) {
        return repository.findByUsername(userName);
    }

    @Override
    public List<Account> getAll() {
        return repository.findAll();
    }

    @Override
    public void save(Account account) {
        repository.save(account);
    }
    
}
