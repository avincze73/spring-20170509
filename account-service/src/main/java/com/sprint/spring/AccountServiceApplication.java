package com.sprint.spring;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AccountServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccountServiceApplication.class, args);
    }

    @Bean
    CommandLineRunner init(AccountRepository accountRepository) {
        return (args) -> {
            Account a1 = new Account();
            a1.setUsername("user1");
            a1.setPassword("password");
            a1.setFullname("user 1");
            accountRepository.save(a1);

            a1 = new Account();
            a1.setUsername("user2");
            a1.setPassword("password");
            a1.setFullname("user 2");
            accountRepository.save(a1);
        };

    }

}
