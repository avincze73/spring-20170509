package com.sprint.spring.springbootanagram;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

@SpringBootApplication
@EnableEurekaClient
@EnableRabbit
public class SpringBootAnagramApplication {

    public static final String EXCHANGE_NAME = "anagramExchange";
    public static final String QUEUE_NAME = "anagramQueue";
    public static final String ROUTING_KEY = "anagram.key";

    public static void main(String[] args) {
        SpringApplication.run(SpringBootAnagramApplication.class, args);
    }

    @Bean
    CommandLineRunner init(AccountRepository accountRepository) {
        return (args) -> {
            Account a1 = new Account();
            a1.setUsername("user1");
            a1.setPassword(new Md5PasswordEncoder().encodePassword("password", null));
            a1.setFullname("user 1");
            accountRepository.save(a1);

            a1 = new Account();
            a1.setUsername("user2");
            a1.setPassword(new Md5PasswordEncoder().encodePassword("password", null));
            a1.setFullname("user 2");
            accountRepository.save(a1);
        };

    }

    
    
      @Bean
    public TopicExchange appExchange() {
        return new TopicExchange(EXCHANGE_NAME);
    }

    @Bean
    public Queue appQueue() {
        return new Queue(QUEUE_NAME);
    }

    @Bean
    public Binding declareBindingGeneric() {
        return BindingBuilder.bind(appQueue()).to(appExchange()).with(ROUTING_KEY);
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
        return new MappingJackson2MessageConverter();
    }

    @Bean
    public DefaultMessageHandlerMethodFactory messageHandlerMethodFactory() {
        DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
        factory.setMessageConverter(consumerJackson2MessageConverter());
        return factory;
    }


}
