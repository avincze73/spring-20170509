/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.springbootanagram;

import com.sprint.spring.spring.anagram.common.AnagramResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author avincze
 */
@RestController
public class AnagramController {

    @Autowired
    private ResourceLoader resourceLoader;

    private final RabbitTemplate rabbitTemplate;

    public AnagramController(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @RequestMapping(value = "/anagram/{word}", method = RequestMethod.GET)
    public ResponseEntity<AnagramResponse> find(@PathVariable("word") String word) {
        AnagramResponse anagramResponse = new AnagramResponse(word);
        Resource resource = resourceLoader.getResource("classpath:/static/dictionary.txt");
        String ordered = lexicographicalOrder(word);

        try (Stream<String> stream = Files.lines(Paths.get(resource.getURI()))) {
            List<String> result = stream.filter(s -> lexicographicalOrder(s).equals(ordered))
                    .collect(Collectors.toList());
            anagramResponse.setAnagramList(result);
        } catch (IOException e) {
            e.printStackTrace();
        }

        rabbitTemplate.convertAndSend(SpringBootAnagramApplication.EXCHANGE_NAME, SpringBootAnagramApplication.ROUTING_KEY, anagramResponse);
        return new ResponseEntity<>(anagramResponse, HttpStatus.OK);
    }

    private String lexicographicalOrder(String input) {
        return input.chars()
                .sorted()
                .mapToObj(i -> (char) i)
                .map(String::valueOf)
                .collect(Collectors.joining());
    }

}
