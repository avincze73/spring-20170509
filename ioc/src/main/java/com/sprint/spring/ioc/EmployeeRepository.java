package com.sprint.spring.ioc;

import java.util.List;
import java.util.Set;

public interface EmployeeRepository {
	void add(Employee employee);
	void update(Employee employee);
	void delete(Employee employee);
	Set<Employee> getAll();

}
