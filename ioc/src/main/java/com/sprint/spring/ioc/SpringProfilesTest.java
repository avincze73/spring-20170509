/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sprint.spring.ioc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author avincze
 */
@Component
public class SpringProfilesTest {

    @Autowired
    DatasourceConfig datasourceConfig;

   
    public void setupDatasource() {
        datasourceConfig.setup();
    }
}
