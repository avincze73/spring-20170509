package com.sprint.spring.ioc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class MyConfig2 {

	@Bean
	public ResourceBundleMessageSource resourceBundleMessageSource() {
		ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		source.addBasenames("employee");
		return source;
	}
	
	@Bean
	public Employee employee1(){
		Employee emp = new Employee();
		emp.setId(Integer.parseInt(resourceBundleMessageSource().getMessage("employee.id", null, "1", null)));
		//emp.setFirstName(resourceBundleMessageSource().getMessage("employee.firstName", null, "f", null));
		emp.setLastName(resourceBundleMessageSource().getMessage("employee.lastName", null, "f", null));
		//emp.setFirstName(resourceBundleMessageSource().getMessage("employee.firstName", null, "f", null));
		emp.setPassword(resourceBundleMessageSource().getMessage("employee.password", null, "f", null));
		emp.setTitle(resourceBundleMessageSource().getMessage("employee.title", null, "f", null));
		emp.setSalary(Double.parseDouble(resourceBundleMessageSource().getMessage("employee.id", null, "1", null)));
		
		return emp;
	}
}
