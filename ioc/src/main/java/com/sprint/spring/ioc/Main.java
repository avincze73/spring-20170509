package com.sprint.spring.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext context = 
                        new ClassPathXmlApplicationContext("beans.xml");
		EmployeeService employeeService = 
                        context.getBean("employeeService",EmployeeService.class);
		employeeService.getAll().forEach(System.out::println);
//		
//		employeeService.delete(1);
//		employeeService.getAll().forEach(System.out::println);
//		
//		employeeService.delete(2);
//		employeeService.getAll().forEach(System.out::println);
//		
//		employeeService.delete(3);
//		employeeService.getAll().forEach(System.out::println);
//		
//		employeeService.add(context.getBean("emp1", Employee.class));
//		employeeService.add(context.getBean("emp2", Employee.class));
//		employeeService.add(context.getBean("emp3", Employee.class));
//		employeeService.getAll().forEach(System.out::println);
		
		
		ApplicationContext context2 = new AnnotationConfigApplicationContext(MyConfig.class);
		EmployeeService employeeService2 = context2.getBean("employeeService",EmployeeService.class);
		System.out.println(employeeService2.hashCode());
		employeeService2.getAll().forEach(System.out::println);
                 
                context2.getBean(SpringProfilesTest.class).setupDatasource();
                
		
//		ApplicationContext context2 = new AnnotationConfigApplicationContext(MyConfig2.class);
//		Employee employee1 = context2.getBean("employee1",Employee.class);
//		//System.out.println(employeeService2.hashCode());
//		System.out.println(employee1.getFirstName());
		

	}

}
