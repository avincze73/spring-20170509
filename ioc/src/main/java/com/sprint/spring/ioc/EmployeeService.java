package com.sprint.spring.ioc;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope(scopeName="prototype")
public class EmployeeService {
	
	@Autowired
	@Qualifier("inMemoryRepository")
	private EmployeeRepository employeeRepository;

	public void setEmployeeRepository(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;
	}
	
	public EmployeeService(){
		System.out.println("aaa" +  this.hashCode());
	}

	public void add(Employee employee) {
		// TODO Auto-generated method stub
		employeeRepository.add(employee);
	}

	public void update(Employee employee) {
		// TODO Auto-generated method stub
		employeeRepository.update(employee);
	}

	public void delete(Employee employee) {
		// TODO Auto-generated method stub
		employeeRepository.delete(employee);
	}

	public Set<Employee> getAll() {
		// TODO Auto-generated method stub
		return employeeRepository.getAll();
	}
	
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		Employee emp = new Employee();
		emp.setId(id);
		employeeRepository.delete(emp);
	}

}
