package com.sprint.spring.ioc;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

@Repository
public class InMemoryEmployeeRepository implements EmployeeRepository {

	private Set<Employee> database;

	

	public void setDatabase(Set<Employee> database) {
		this.database = database;
	}

	public void add(Employee employee) {
		// TODO Auto-generated method stub
		database.add(employee);

	}

	public void update(Employee employee) {
		// TODO Auto-generated method stub
		database.remove(employee);
		database.add(employee);
	}

	public void delete(Employee employee) {
		// TODO Auto-generated method stub
		database.remove(employee);
	}
	
	

	public Set<Employee> getAll() {
		// TODO Auto-generated method stub
		return database;
	}

}
