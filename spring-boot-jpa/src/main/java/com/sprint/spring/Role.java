package com.sprint.spring;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Role extends EvoEntity {

	@Basic(optional = false)
	@Column(name = "name")
	private String name;

	
	public Role() {
		// TODO Auto-generated constructor stub
		this(null);
	}
	
	public Role(String name) {
		super();
		this.name = name;
	}
	
}
