package com.sprint.spring;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringBootJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJpaApplication.class, args);
	}
        
        @Bean
        CommandLineRunner init(RoleRepository repository){
            return (args)->{
                Role role = new Role("admin");
                Role role2 = new Role("user");
                repository.save(role);
                repository.save(role2);
            };
        }
}
